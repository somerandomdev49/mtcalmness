#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include <stdio.h>

int main() {
	if(!glfwInit()) {
		printf("Failed to initialize GLFW.\n");
		return 1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow *window = glfwCreateWindow(800, 600, "example", NULL, NULL);
	if(!window) {
		printf("Failed to create a window\n");
		return 1;
	}

	glfwMakeContextCurrent(window);
	if(gl3wInit()) {
		printf("Failed to initialize OpenGL\n");
		return 1;
	}

	printf("OpenGL %s, GLSL %s\n", glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));

	while(!glfwWindowShouldClose(window)) {
		glfwPollEvents();

		glfwSwapBuffers(window);
	}

	return 0;

}
