from dataclasses import dataclass
from io import TextIOWrapper
import os, sys, os.path
import string
import random
from typing import Callable, Iterator, Literal

from util import Buffered, FileLikeIterator, random_string

@dataclass
class Location:
	fname: str
	line: int
	col: int

@dataclass
class LocationSpan:
	begin: Location
	end: Location

	def length(self):
		return (self.end.col - self.begin.col
			   +self.end.line - self.begin.line)
	
	def is_multiline(self) -> bool:
		return self.end.line != self.begin.line
	
	def __getitem__(self, b: Literal[0, 1]) -> Location:
		return self.begin if b == 0 else self.end


@dataclass
class Source:
    fname: str
    lines: list[str]


CACHED_FILE_SOURCES: dict[str, Source] = {}


def _read_file_src(name: str) -> None:
	try:
		with open(name, "r") as f:
			CACHED_FILE_SOURCES[name] = Source(name, f.readlines())
	except:
		CACHED_FILE_SOURCES[name] = Source(name, "")


def _get_file_src(name: str) -> Source:
    if name not in CACHED_FILE_SOURCES:
        _read_file_src(name)
    return CACHED_FILE_SOURCES[name]


@dataclass
class Token:
	type: str
	value: str
	span: LocationSpan

@dataclass
class AST:
	span: LocationSpan

	def gen(self, _ctx: "Generator"):
		raise NotImplementedError(f"abstract ({self.__class__.__name__})")


@dataclass
class AST_Use(AST):
	name: str

	def gen_directive(self, ctx: "Generator"):
		if self.name.startswith("!include"):
			_, fname = self.name.split(':')
			ctx.included_ofiles.add(fname)
		elif self.name.startswith("!cextern"):
			_, cname, name = self.name.split(':')
			ctx.externs[cname] = name
		else:
			ctx.error("Bad 'use' directive", where=self.span)

	def gen_include(self, ctx: "Generator"):
		try:
			with open(self.name + ".mw") as f:
				ctx.gen(Parser(Lex(f).tokens()).parse_program())
		except IOError:
			ctx.error(f"Error while opening file '{self.name}'!",
				where=self.span)

	def gen(self, ctx: "Generator"):
		if self.name.startswith("!"):
			self.gen_directive(ctx)
		else:
			self.gen_include(ctx)

@dataclass
class AST_Function(AST):
	name: str
	args: list[str]
	body: AST

	def gen(self, ctx: "Generator"):
		ctx.emit(f"[global {ctx.mangle(self.name)}]");
		ctx.emit(f"{ctx.mangle(self.name)}:");
		

@dataclass
class AST_If(AST):
	cond: AST
	body: AST
	othr: AST


@dataclass
class AST_While(AST):
	cond: AST
	body: AST


@dataclass
class AST_Do(AST):
	body: list[AST]


@dataclass
class AST_Bind(AST):
	name: str
	value: AST
	
	def gen(self, ctx: "Generator"):
		if self.name.startswith("!"):
			self.gen_directive(ctx)
		else:
			self.gen_include(ctx)

@dataclass
class AST_Call(AST):
	what: AST
	args: list[AST]


@dataclass
class AST_Return(AST):
	value: AST


@dataclass
class AST_Iden(AST):
	name: str


@dataclass
class AST_Str(AST):
	value: str


@dataclass
class AST_Int(AST):
	value: int
	

@dataclass
class AST_Op(AST):
	lhs: AST
	rhs: AST
	opr: str


def generic_error(
	self,
	msg: str,
	*,
	fatal=False,
	where: LocationSpan = None,
	warning: bool = False,
):
	_tab_string = "\033[0;90m➝   \033[0;0m"
	_tab_string_len = 4

	_spc_string = "\033[0;90m·\033[0;0m"
	_spc_string_len = 1

	# TODO: Handle multiline errors
	if self.error_count > self.error_limit + 1:
		print(f"Error count limit exceeded ({self.error_limit})")
		exit(1)

	txt = f"\033[1;33mWarning" if warning else f"\033[1;31mError"
	self.error_count += 0 if warning else 1
	print(txt + ": \033[1;37m" + msg + "\033[0m")
	if where and where[0].fname != "?":
		src = _get_file_src(where[0].fname)
		ln = f"{where[0].fname}:{where[0].line} | "

		try:
			original_code = src.lines[where[0].line - 1][:-1]
		except:
			print("\033[0;90m?code missing?\033[0;0m")
			if fatal:
				exit(1)
			return

		def text_length_addend(before: int) -> int:
			return original_code[:before].count("\t") * (
				_tab_string_len - 1
			) + original_code[:before].count(" ") * (
				_spc_string_len - 1
			)

		def text_col(actual: int) -> int:
			return actual + text_length_addend(actual)

		text_code = (original_code
			.replace(" ", _spc_string)
			.replace("\t", _tab_string)
		)

		print(ln + f"{text_code}")
		print((len(ln)) * " ", end="\033[0;36m")
		if where[1] is not None:
			print(text_col(where[0].col) * " ", end="")
			print((text_col(where[1].col) - text_col(where[0].col)) * "~")
		else:
			print(len(text_code) * "~")
		print("\033[0;0m", end="")

	if fatal:
		exit(1)

class Generator:
	def __init__(self):
		self.included_ofiles = set()
		self.externs = {}

		self.error_count = 0
		self.error_limit = 15
	
	def error(
		self,
		msg: str,
		*,
		fatal=False,
		where: LocationSpan = None,
		warning: bool = False,
	):
		generic_error(self, msg, fatal=fatal, where=where, warning=warning)

	def mangle(self, name: str):
		return "_" + name

	def gen(self, program: list[AST]):
		for node in program:
			node.gen(self)

def print_ast(ast):
	import re
	STR_REPLACE = (
		r"span=LocationSpan\(begin=Location\([^)]*\), end=Location\([^)]*\)\), "
	)
	s = re.sub(STR_REPLACE, "", repr(ast))
	try:
		from black import format_str, FileMode
		res = format_str(s, mode=FileMode())
		print(res.replace("    ", " "))
	except ModuleNotFoundError:
		print(s)

def gen(ast):
	Generator().gen(ast)


class Parser:
	def __init__(self, toks: list[Token]):
		self.toks = toks
		self._buf = Buffered(FileLikeIterator(iter(self.toks)))
		self.error_count = 0
		self.error_limit = 20


	def parse_atom(self) -> AST:
		t = self._read()
		if t.type == "iden": return AST_Iden(t.span, t.value)
		elif t.type == "num": return AST_Int(t.span, int(t.value))
		elif t.type == "str": return AST_Str(t.span, t.value)
		else:
			self._error("Expected an atom (identifier, number, parenthesis...)",
				where=t.span)
			return AST_Iden(t.span, t.value)
	
	def parse_prefix(self) -> AST:
		if self._peek().type == "!":
			t = self._read()
			a = self.parse_atom()
			return AST_Op(LocationSpan(t.span.begin, a.span.end), a, None, "!")
		
		return self.parse_atom()

	def parse_suffix(self) -> AST:
		a = self.parse_prefix()
		
		while self._peek().type == "(":
			loc, args = self.parse_args()
			a = AST_Call(LocationSpan(a.span.begin, loc.end), a, args)

		return a

	def get_generic_op_parser(self, next: Callable[[], AST], ops: list[str]) -> Callable[[], AST]:
		def inner():
			lhs = next()
			while self._peek().type in ops:
				opr = self._read().type
				rhs = next()
				lhs = AST_Op(
					LocationSpan(lhs.span.begin, rhs.span.end),
					lhs, rhs, opr
				)
			
			return lhs
		return inner

	def get_op_tree_parser(self, op: str = "eql") -> Callable[[], AST]:
		if op == "mul":
			return self.get_generic_op_parser(self.parse_suffix, list("*/"))
		elif op == "add":
			return self.get_generic_op_parser(self.get_op_tree_parser("mul"), list("+-"))
		elif op == "eql":
			return self.get_generic_op_parser(self.get_op_tree_parser("add"), ["!=", "=="])
		else:
			raise NotImplementedError(f"get_op_tree_parser('{op}')")

	def parse_expression(self) -> AST:
		return self.get_op_tree_parser()()

	def parse_args(self) -> tuple[LocationSpan, list[AST]]:
		begin = self._peek().span.begin

		if not self._peek().type == "(":
			self._error("Expected an opening parenthesis for the function call arguments.")
		else:
			self._read()
		
		args: list[AST] = []
		
		while self._peek().type != ")":
			arg = self.parse_expression()
			args.append(arg)
			end = args[-1].span.end
			if self._peek().type == ",":
				self._read()

		
		if not self._peek().type == ")":
			self._error("Expected an closing parenthesis for the function call arguments.")
		else:
			end = self._peek().span.end
			string.ascii_lowercase
			self._read()
		
		return LocationSpan(begin, end), args

	def parse_arg_names(self) -> tuple[LocationSpan, list[str]]:
		begin = self._peek().span.begin

		if not self._peek().type == "(":
			self._error("Expected an opening parenthesis for the function call arguments.")
		else:
			self._read()
		
		args: list[str] = []
		
		while self._peek().type != ")":
			name_t = self._read()
			if name_t.type != "iden":
				self._error("Expected an identifier for the argument name!",
					where=name_t.span)
				name = "error" + random_string()
			else:
				name = name_t.value

			args.append(name)
			end = name_t.span.end

			if self._peek().type == ",":
				self._read()

		
		if not self._peek().type == ")":
			self._error("Expected an closing parenthesis for the function call arguments.")
		else:
			end = self._peek().span.end
			string.ascii_lowercase
			self._read()
		
		return LocationSpan(begin, end), args

	def parse_statement(self) -> AST:
		if self._peek().type == "iden":
			kws = {
				"do": self.parse_do,
				"bind": self.parse_bind,
				"if": self.parse_if,
				"return": self.parse_return,
				"while": self.parse_while,
			}
			if self._peek().value in kws:
				return kws[self._peek().value](self._read())

		elif self._peek().type == "eof":
			self._error("Unexpected EOF.", where=self.toks[-2].span)

		return self.parse_expression()

	def parse_do(self, kw: Token) -> AST:
		nodes = []
		while not (self._peek().type == "iden" and self._peek().value == "end"):
			nodes.append(self.parse_statement())

		end_tok = self._read()
		
		return AST_Do(LocationSpan(kw.span.begin, end_tok.span.end), nodes)

	def parse_if(self, kw: Token) -> AST:
		cond = self.parse_expression()

		if not (self._peek().type == "iden" and self._peek().value == "then"):
			self._error("Expected 'then' keyword after if!",
				where=self._peek().span)
		else:
			self._read()

		body = self.parse_statement()
		othr = None
		end = body.span.end

		if self._peek().type == "iden" and self._peek().value == "else":
			self._read()
			othr = self.parse_statement()
			end = othr.span.end
	
		return AST_If(LocationSpan(kw.span.begin, end), cond, body, othr)

	def parse_return(self, kw: Token) -> AST:
		value = self.parse_expression()
	
		return AST_Return(LocationSpan(kw.span.begin, value.span.end), value)

	def parse_while(self, kw: Token) -> AST:
		cond = self.parse_expression()
		body = self.parse_statement()
	
		return AST_While(LocationSpan(kw.span.begin, body.span.end), cond, body)

	def parse_function(self, kw: Token) -> AST_Function:
		name_t = self._read()
		if name_t.type != "iden":
			self._error("Expected an identifier for the function name!",
				where=name_t.span)
			name = "error" + random_string()
		else:
			name = name_t.value

		_, args = self.parse_arg_names()
		body = self.parse_statement()

		return AST_Function(
			LocationSpan(kw.span.begin, body.span.end),
			name, args, body
		)

	def parse_bind(self, kw: Token) -> AST_Bind:
		name_t = self._read()
		if name_t.type != "iden":
			self._error("Expected the binding's name!",
				where=name_t.span)
			name = "error" + random_string()
		else:
			name = name_t.value
		
		value = self.parse_expression()

		return AST_Bind(LocationSpan(kw.span.begin, value.span.end), name, value)
		
	def parse_use(self, kw: Token) -> AST_Use:
		name_t = self._read()
		if name_t.type != "str":
			self._error("Expected a string for the module name!",
				where=name_t.span)
			name = "error" + random_string()
		else:
			name = name_t.value

		return AST_Use(LocationSpan(kw.span.begin, name_t.span.end), name)

	def parse_toplevel(self) -> AST:
		t = self._read()
		if t.type == "iden" and t.value == "function":
			return self.parse_function(t)
		elif t.type == "iden" and t.value == "bind":
			return self.parse_bind(t)
		elif t.type == "iden" and t.value == "use":
			return self.parse_use(t)
		else:
			self._error("Expected a top-level expression (function, bind, use)",
				where=t.span)

	def parse_program(self) -> list[AST]:
		l = []
		while self._peek().type != "eof":
			l.append(self.parse_toplevel())
		
		return l

	def _error(
		self,
		msg: str, *,
		fatal: bool = False,
		where: LocationSpan = None,
		warning: bool = False
	):
		generic_error(self, msg, fatal=fatal, where=where, warning=warning)
	
	def __input_common(self, r: list[Token]) -> Token:
		return r[-1] if r else Token("eof", "eof", LocationSpan(
			Location("<error>", 0, 0),
			Location("<error>", 0, 0),
		))
			
	def _read(self, count=1) -> Token:
		return self.__input_common(self._buf.read(count))

	def _peek(self, count=1) -> Token:
		return self.__input_common(self._buf.peek(count))
	


def ast(toks: list[Token]):
	# print(*toks, sep='\n')
	return Parser(toks).parse_program()

 
class Lex:
	def __init__(self, file: TextIOWrapper):
		self._fname = file.name
		self._file = file
		self._col = 0
		self._peek_buf: list[str] = []
		self.had_errors = False
		self._ignore_comments = False
		self._lineno = 1
	

	def tokens(self) -> Iterator[Token]:
		while True:
			self._skip_while(self._peek(), self._isspace)

			c = self._read()
			if not c: break

			pos = self._col - 1
			if c.isalpha() or c == "_":
				s = self._read_while(c, self._isiden)
				yield Token("iden", s, self._loc(pos, self._col, self._lineno))
			elif c.isdigit():
				s = self._read_while(c, self._isnumber)
				yield Token("num", s, self._loc(pos, self._col, self._lineno))
			elif c == '"':
				buf = ""
				while self._peek() != '"':
					c = self._read()
					if c == '\\':
						c = self._read()
						if c == 'n': buf += '\n'
						elif c == 'r': buf += '\r'
						elif c == 't': buf += '\t'
						else:
							print(f"Lexer error (TODO): Invalid escape sequence: '\\{c}'")
					else:
						buf += c
						
				self._read()

				yield Token("str", buf, self._loc(pos, self._col, self._lineno))
			elif c == '=' and self._peek() == '=':
				self._read()
				yield Token("==", "==", self._loc(pos, self._col, self._lineno))
			else:
				yield Token(c, c, self._loc(pos, self._col, self._lineno))

		yield Token("eof", "eof", self._loc(self._col - 1, self._col, self._lineno))
	
	def _loc(
		self, col1: int, col2: int, line1: int, line2: int = None
	) -> LocationSpan:
		if line2 is None:
			line2 = line1
		return LocationSpan(
			Location(self._fname, line1, col1),
			Location(self._fname, line2, col2),
		)

	def _read_impl(self, size: int, inc: bool) -> str:
		b = self._file.read(size)
		if inc:
			self._col += size
			if b == "\n":
				self._lineno += 1
				self._col = 0

		return "" if not b else b[-1]

	def _read(self, size=1, _peeks=False) -> str:
		if not _peeks and len(self._peek_buf) != 0:
			chrs = self._peek_buf[:size]
			self._peek_buf = self._peek_buf[size:]
			return chrs[-1]

		if self._ignore_comments:
			return self._read_impl(size, not _peeks)
		else:
			c = ""
			for _ in range(size):
				c = self._read_impl(1, not _peeks)
				if c == "#":
					while c != "\n" and c != "":
						c = self._read_impl(1, not _peeks)
			
			return c

	def _peek(self, size=1) -> str:
		if len(self._peek_buf) != 0:
			chrs = self._peek_buf[:size]
			self._peek_buf = self._peek_buf[size:]
			return chrs[-1]

		prev_pos = self._file.tell()
		b = self._read(size, _peeks = True)
		self._file.seek(prev_pos)
		return "" if not b else b

	def _skip_while(self, c: str, f: Callable[[str], bool]) -> int:
		if not f(c):
			return ""
		count = 1
		while True:
			if self._peek() == "" or not f(self._peek()):
				break
			self._read()
			count += 1

		return count

	def _read_while2(self, c: str, f: Callable[[str, str], bool]) -> str:
		if not f(c, self._peek()):
			return ""

		buffer = [c]
		while True:
			if self._peek() == "" or not f(self._peek(), self._peek(2)):
				break
			buffer.append(self._read())

		return "".join(buffer)

	def _read_while(self, c: str, f: Callable[[str], bool]) -> str:
		return self._read_while2(c, lambda x, _: f(x))

	def _isspace(self, c: str) -> bool:
		return c.isspace()

	def _isiden(self, c: str) -> bool:
		return c.isalnum() or c == "_"

	def _isnumber(self, c: str) -> bool:
		return c.isdigit() or c == "_"


def lex(fname: str) -> Iterator[Token]:
	with open(fname) as f:
		return list(Lex(f).tokens())


@(lambda f: exit(f()))
def main() -> int:
	if len(sys.argv) < 2:
		return 1

	fname_in = sys.argv[1]
	fname_out = os.path.splitext(sys.argv[1])[0] + ".o"

	gen(ast(lex(fname_in)))
	
	return 0

