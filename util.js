
module.exports = {
	match: (val, opts, def) => val in opts ? opts[val](val) : (def ? def(val) : undefined),
	fmatch: (val, opts, def) =>
		(sel => sel.length ? sel[0][1](val) : (def ? def(val) : undefined))
		(/* sel = */ opts.filter(f => f[0](val))),
	collect: () => 
}
