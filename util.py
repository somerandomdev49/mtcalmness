from typing import Generic, Iterator, TypeVar
import random, string, uuid

def random_string(l: int = 12) -> str:
	return ''.join(str(uuid.uuid4()).split('-'))

T = TypeVar("T")

class ReadableFileLike(Generic[T]):
    def read(self, count=1) -> T:
        yield NotImplementedError("FileLike is an abstract class.")


class Buffered(Generic[T]):
    def __init__(self, it: ReadableFileLike[T]) -> None:
        self._it = it
        self._buffer: list[T] = []

    def peek(self, count=1) -> list[T]:
        if len(self._buffer) < count:
            self._buffer += self._it.read(count - len(self._buffer))
        return self._buffer

    def read(self, count=1) -> list[T]:
        if len(self._buffer) > count:
            vals = self._buffer[:count]
            del self._buffer[:count]
            return vals
        else:
            left = count - len(self._buffer)
            vals = self._buffer.copy()
            self._buffer.clear()
            return vals + self._it.read(left)


class FileLikeIterator(ReadableFileLike[list[T]]):
    def __init__(self, base: Iterator[T]) -> None:
        self._base = base

    def read(self, count=1) -> list[T]:
        vals = []
        for _ in range(count):
            try:
                vals.append(next(self._base))
            except StopIteration:
                break

        return vals
